#include "RE0HDTrainer.h"
#include <QtWidgets/QApplication>
#include <iostream>
#define PSAPI_VERSION 1
#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include <psapi.h>
#include <string>
#include <thread>
#define _CRT_SECURE_NO_WARNINGS
#define UNINITIALIZED 0xFFFFFFFF
#include <iomanip>
#include <handleapi.h>
#include <TlHelp32.h> //PROCESSENTRY
// To ensure correct resolution of symbols, add Psapi.lib to TARGETLIBS
#pragma comment(lib, "psapi.lib")

#define MAX_LOADSTRING 100
#define FALSE 0

DWORD GetGameBaseAddress()
{
	DWORD GameBaseAddress = NULL;
	//Enumerate all process IDs and store them to an array.
	DWORD aProcesses[1024], cbNeeded, cProcesses;
	EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded);
	cProcesses = cbNeeded / sizeof(DWORD);
	unsigned int i = 0;
	for (i = 0; i < cProcesses; i++)
	{
		DWORD processID = aProcesses[i];
		TCHAR szProcessName[MAX_PATH] = TEXT("<unknown>");

		// Get a handle to the process.

		HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION |
			PROCESS_VM_READ,
			FALSE, processID);

		// Get the process name.

		if (GetModuleBaseNameW(hProcess, 0, szProcessName, sizeof(szProcessName) / sizeof(TCHAR)))
		{
			// Check if the process has the name of the game.
			TCHAR szTargetProcessName[MAX_PATH] = TEXT("re0hd.exe");
			if (*szTargetProcessName == *szProcessName)
			{
				// Get a snapshot handle for the process.
				HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, processID);
				//Define the structure to store the exe/first module information to.
				MODULEENTRY32 me32;
				me32.dwSize = sizeof(MODULEENTRY32);
				me32.th32ModuleID = 1;
				me32.th32ProcessID = processID;
				me32.GlblcntUsage = 0xFFFF;
				me32.ProccntUsage = 0xFFFF;
				//Fetch and store the information of the first module/exe and extract the base address from it.
				Module32First(hSnapshot, &me32);
				GameBaseAddress = reinterpret_cast<DWORD>(me32.modBaseAddr);
				// Release the snapshot handle to the process.
				CloseHandle(hSnapshot);
			}
		}

		// Release the handle to the process.
		if (GameBaseAddress == NULL)
		{
			CloseHandle(hProcess);
		}
		else
		{
			//save hProcess handle to struct
		}
	}
	if (GameBaseAddress != NULL)
	{
		return GameBaseAddress;
	}
	else
	{
		return NULL;
	}
}


BOOL GetGameBaseAddressLoop()
{
	for (; ; )
	{
		DWORD NewGameBaseAddress = GetGameBaseAddress();
		if (NewGameBaseAddress == NULL)
		{
			std::cout << "Game not running" << std::endl;
			Sleep(100);
			GetGameBaseAddress();
		}
		else
		{
			std::cout << "Game running" << std::endl;
			//save	NewGameBaseAddress to struct	
			DWORD Test = NewGameBaseAddress;
			Sleep(1000);
		}
	}
}

int main(int argc, char *argv[])
{
	std::thread Thread1(GetGameBaseAddressLoop);
	QApplication a(argc, argv);
	TrainerRE0 w;
	w.show();
	return a.exec();
}

